import React, {Component} from 'react';
import axios from '../../axios-contacts';
import Contacts from "../../components/Contacts/Contacts";

import './Home.css';
import ContactItem from "../../components/ContactItem/ContactItem";
import Form from "../../components/Form/Form";

class Home extends Component {
  state = {
    contacts: null, // here will be an array with contacts-objects
    formVisible: false,
    contact: {}, // here will be and object with one contact info for changing
    index: null, // here will be the index on contact for edit
  };

  componentDidMount() {
    const contacts = JSON.parse(localStorage.getItem('contacts'));
    if (contacts) {
      this.setState({contacts});
    } else {
      this.getContactsFromApi();
    }
  }

  // Function for getting contacts data from API, set it to localStorage and to state.

  getContactsFromApi = () => {
    axios.get('').then(
      response => {
        const contacts = response.data;
        localStorage.setItem('contacts', JSON.stringify(contacts)); // here we save contacts-array to local storage

        this.setState({contacts});
      },
      error => {
        console.log(error);
      }
    );
  };

  // Function for getting form for edit contact

  formVisibleToggle = () => {
    this.setState({formVisible: !this.state.formVisible});
  };

  // Function for getting contact info to form

  getContactInfoToForm = (contact, index) => {
    // contact - object with data of contact
    // index - index of contact in contacts array.
    this.setState({contact: contact, index: index});
    this.formVisibleToggle();
  };

  // Two-way-binding functions for editing contact

  inputChangeHandler = event => { // function for editing all inputs without address
    this.setState({
      contact: {
        ...this.state.contact,
        [event.target.name]: event.target.value,
      }
    })
  };

  addressInputChangeHandler = event => { // function for editing address-inputs
    this.setState({
      contact: {
        ...this.state.contact,
        address: {
          ...this.state.contact.address,
          [event.target.name]: event.target.value,
        }
      }
    })
  };

  // Function for completing edit

  completeEditContact = event => {
    event.preventDefault();
    const contacts = JSON.parse(localStorage.getItem('contacts'));

    contacts[this.state.index] = this.state.contact; // here we changing one contact from localStorage for saving it there

    localStorage.setItem('contacts', JSON.stringify(contacts)); //here we update localStorage
    this.setState({contacts}); // here we update state
    this.formVisibleToggle();
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // console.log(JSON.parse(localStorage.getItem('contacts')));
  }

  render() {
    if (!this.state.contacts) {
      // before data will come from API, we return pre-loader
      return <div>Loading...</div>
    }
    return (
      <div className="Home">
        <Contacts>
          {this.state.contacts.map((contact, index) => {
            // 'contact' - object with all data for one contact
            return (
              <ContactItem
                onCardClick={() => this.getContactInfoToForm(contact, index)}
                key={contact.id} avatar={contact.avatar}
                name={contact.name} username={contact.username}
                phone={contact.phone} email={contact.email}
                website={contact.website}
                address={contact.address}
              />
            )
          })}
        </Contacts>
        {this.state.formVisible ? <Form
          onCloseClick={this.formVisibleToggle}
          nameValue={this.state.contact.name}
          usernameValue={this.state.contact.username}
          phoneValue={this.state.contact.phone}
          emailValue={this.state.contact.email}
          websiteValue={this.state.contact.website}
          cityValue={this.state.contact.address.city}
          countryValue={this.state.contact.address.country}
          change={event => this.inputChangeHandler(event)}
          changeAddress={event => this.addressInputChangeHandler(event)}
          onEditClick={event => this.completeEditContact(event)}
        /> : null}
      </div>
    );
  }
}

export default Home;