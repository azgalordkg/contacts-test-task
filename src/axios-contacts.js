import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://demo.sibers.com/users',
});

export default instance;