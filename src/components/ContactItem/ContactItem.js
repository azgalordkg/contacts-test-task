import React, {Fragment} from 'react';

import './ContactItem.css';

const ContactItem = props => (
  <div onClick={props.onCardClick} className="ContactItem">
    <div className="ContactsItemMainInfo">
      <div
        style={{backgroundImage: `url(${props.avatar})`}}
        className="ContactsItemMainInfoAvatar"/>
      <div className="ContactsItemMainInfoText">
        <h5><b>Name:</b> {props.name}</h5>
        <h5><b>Username:</b> {props.username}</h5>
      </div>
    </div>
    <div className="ContactItemAllInfo">
      <span><b>Phone:</b> {props.phone}</span>
      <span><b>E-mail:</b> {props.email}</span>
      <span><b>Website:</b> {props.website}</span>
    </div>
    <div className="ContactItemAllInfo ContactsAddress">
      {props.address ? <Fragment>
        <span><b>City: </b> {props.address.city}</span>
        <span><b>Country: </b> {props.address.country}</span>
      </Fragment> : null}
    </div>
  </div>
);

export default ContactItem;