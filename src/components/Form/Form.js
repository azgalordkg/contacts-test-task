import React, {Fragment} from 'react';

import './Form.css';

const Form = props => (
  <Fragment>
    <div className="FormBackground">
      <div className="FormContainer">
        <h2>Edit contact</h2>
        <form>
          <input
            value={props.nameValue} onChange={props.change}
            type="name" placeholder="Name" name="name"/>
          <input
            value={props.usernameValue} onChange={props.change}
            type="text" placeholder="Username" name="username"/>
          <input
            value={props.phoneValue} onChange={props.change}
            type="text" placeholder="Phone" name="phone"/>
          <input
            value={props.emailValue} onChange={props.change}
            type="email" placeholder="E-mail" name="email"/>
          <input
            value={props.websiteValue} onChange={props.change}
            type="text" placeholder="Website" name="website"/>
          <input
            value={props.cityValue} onChange={props.changeAddress}
            type="text" placeholder="City" name="city"/>
          <input
            value={props.countryValue} onChange={props.changeAddress}
            type="text" placeholder="Country" name="country"/>
          <button onClick={props.onEditClick}>Edit</button>
        </form>
        <button onClick={props.onCloseClick} className="FormClose">X</button>
      </div>
    </div>
  </Fragment>
);

export default Form;