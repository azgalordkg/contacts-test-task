import React from 'react';

import './Contacts.css';

const Contacts = ({children}) => (
  <div className="Contacts">
    {children}
  </div>
);

export default Contacts;