import React, {Fragment} from 'react';

import './Layout.css';

const Layout = ({children}) => (
  <Fragment>
    <header className="LayoutHeader">
      <div className="HeaderLogo"/>
    </header>
    <main className="LayoutContent">
      {children}
    </main>
  </Fragment>
);

export default Layout;